import json
import pandas as pd
from ranking.preprocessing import preprocessing
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import accuracy_score


with open('config/config.json', 'r') as file:
    config = json.load(file)

train = pd.read_pickle(config['train'])
test = pd.read_pickle(config['test'])

train['pr_text'] = train.apply(lambda row: preprocessing(row['text'], row['category']), axis = 1)
test['pr_text'] = test.apply(lambda row: preprocessing(row['text'], row['category']), axis = 1)

print('train statistics')
print(train['category'].value_counts())

embedder = TfidfVectorizer(ngram_range=(1,2))
clf = RandomForestClassifier(n_jobs=5, verbose=True, n_estimators=200)

train_corpus = train['pr_text'].tolist()
train_labels = train['rank'].tolist()

embedder.fit(train_corpus)
embedded_train_corpus = embedder.transform(train_corpus)
clf.fit(embedded_train_corpus, train_labels)

test['new_rank'] = clf.predict(embedder.transform(test['pr_text']))
test = test[['text','predicted_category','category','rank','new_rank','prob','freq']]

print(test.head(2).to_string())

test.to_pickle(config['save']['test'])
pd.to_pickle(clf, config['save']['classifier'])
pd.to_pickle(embedder, config['save']['embedder'])