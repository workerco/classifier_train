import torch
import pandas as pd

from sklearn.model_selection import train_test_split
from sentence_transformers import SentenceTransformer
from torch.utils.data import DataLoader
from catalyst import dl
# custom packages
from categorization.dataset import DialogDatasetEmbedded, custom_collate
from categorization.model import SentenceTagging
from collections import OrderedDict

###
# constants

category2index = {
    'sleep': 1,
    'food': 2,
    'water': 3,
    'stress': 4,
    'workout': 5,
    'weight': 0,
    0: 0
}
#
main_model_params = {
    "gru_input_dim": 769,
    "gru_hidden_dim": 100,
    "num_layers": 2,
    "bidirectional": True,
    "output_dim": 6
}
#
index2category = {category2index[key]: key for key in category2index}
index2category[0] = 'None'


#

###

class SentanceTaggingRunner(dl.Runner):

    def predict_batch(self, batch):
        features, _, categories, user_messages_indexes, seq_lens, _ = batch
        number_of_dialogs = len(seq_lens)

        features = features.to(self.device)
        probas = self.model(features)
        predictions = [probas[idx][torch.Tensor(user_messages_indexes[idx]).long()] \
                       for idx in range(number_of_dialogs)]

        return predictions

    def _handle_batch(self, batch):
        # model train/valid step
        features, _, categories, user_messages_indexes, seq_lens, _ = batch
        predictions = self.predict_batch(batch)
        y_true = [torch.Tensor([category[i] for i in user_messages_indexes[idx]]) \
                  for idx, category in enumerate(categories)]

        y_hat = torch.cat(predictions)
        y_true = torch.cat(y_true).long().to('cuda')

        self.input = {"targets": y_true}
        self.output = {"logits": y_hat}


if __name__ == '__main__':
    text_encoder = SentenceTransformer('distilbert-base-nli-mean-tokens', device='cuda')

    dialogs = pd.read_pickle('data/dialogs.pkl')
    dialogs = pd.DataFrame(dialogs)
    dialogs = dialogs.apply(list, axis=1).tolist()
    dialogs = [[item for item in dialog if item] for dialog in dialogs]

    train, test = train_test_split(dialogs,
                                   random_state=42,
                                   shuffle=True,
                                   test_size=0.2)

    train_dataset = DialogDatasetEmbedded(text_encoder,
                                          category2index,
                                          train,
                                          sender_field="name",
                                          sender_mark="received")

    test_dataset = DialogDatasetEmbedded(text_encoder,
                                         category2index,
                                         test,
                                         sender_field="name",
                                         sender_mark="received")

    train_loader = DataLoader(train_dataset,
                              batch_size=64,
                              collate_fn=custom_collate,
                              shuffle=True)

    test_loader = DataLoader(test_dataset,
                             batch_size=400,
                             collate_fn=custom_collate,
                             shuffle=False)
    loaders = OrderedDict({
        "train": train_loader,
        "valid": test_loader
    })

    main_model = SentenceTagging(
        main_model_params['gru_input_dim'],
        main_model_params['gru_hidden_dim'],
        main_model_params['num_layers'],
        main_model_params['bidirectional'],
        main_model_params['output_dim']
    ).to('cuda')

    runner = SentanceTaggingRunner(device="cuda")
    optimizer = torch.optim.Adam(main_model.parameters(), lr=0.001)
    scheduler = torch.optim.lr_scheduler.ReduceLROnPlateau(optimizer, threshold=0.01)

    criterion = torch.nn.NLLLoss(weight=torch.Tensor([1, 3, 1.3, 2.4, 2.4, 1.2]).cuda(), reduction='mean')

    # criterion = catalyst.contrib.nn.criterion.focal.FocalLossMultiClass()


    runner.train(
        model=main_model,
        optimizer=optimizer,
        loaders=loaders,
        criterion=criterion,
        scheduler=scheduler,
        logdir="./logs/wighted_bce",
        num_epochs=10,
        verbose=True,
        load_best_on_end=True,
        callbacks=OrderedDict({

            "criterion": dl.CriterionCallback(  # special Callback for criterion computation
                input_key="targets",  # `input_key` specifies correct labels (or `y_true`) from `runner.input`
                output_key="logits",  # `output_key` specifies model predictions (`y_pred`) from `runner.output`
                prefix="loss",  # `prefix` - key to use with `runner.batch_metrics`
            ),

            # "lr_finder": catalyst.dl.callbacks.scheduler.LRFinder(1e-5,
            #                                                       num_steps=3),

            "metric": dl.AccuracyCallback(num_classes=6,
                                          accuracy_args=[1, 2]),

            "confusion_matrix": dl.ConfusionMatrixCallback(num_classes=6,
                                                           class_names=["none",
                                                                        "sleep",
                                                                        "food",
                                                                        "water",
                                                                        "stress",
                                                                        "workout"]),
            "optimizer": dl.OptimizerCallback(metric_key="loss",
                                              accumulation_steps=1,
                                              grad_clip_params=None),

            "scheduler": dl.SchedulerCallback(reduced_metric="loss")

        })
    )
