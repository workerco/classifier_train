from torch import nn

class SentenceTagging(nn.Module):
    def __init__(self,input_size,
                 hidden_size,
                 num_layers,
                 bidirectional,
                 output_dim):

        super(SentenceTagging, self).__init__()

        self.rnn = nn.GRU(input_size=input_size,
                          hidden_size = hidden_size,
                          num_layers = num_layers,
                          bidirectional = bidirectional,
                          batch_first=True)

        self.logsoftmax = nn.LogSoftmax(dim=2)

        if bidirectional:
            self.decide = nn.Linear(hidden_size*2,output_dim)
            self.batch_norm = nn.BatchNorm1d(hidden_size * 2)
        else:
            self.decide = nn.Linear(hidden_size, output_dim)
            self.batch_norm = nn.BatchNorm1d(hidden_size)

        self.dropout = nn.Dropout()

    def forward(self, embeddings):
        # embeddings (batch_size, seq_len, input)

        sentence_embeddings = self.rnn(self.dropout(embeddings))[0]
        shape = sentence_embeddings.shape
        sentence_embeddings = sentence_embeddings.reshape(shape[0],shape[2],shape[1])

        sentence_embeddings = self.dropout(self.batch_norm(sentence_embeddings).reshape(shape))


        features = self.decide(sentence_embeddings)

        return self.logsoftmax(features)
