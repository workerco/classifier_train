import pandas as pd
import numpy as np
import tqdm
import json
import torch
from sentence_transformers import SentenceTransformer
from sklearn.model_selection import train_test_split
from torch.utils.data import Dataset, DataLoader
from pprint import pprint


def encode_features(text_encoder, texts, sender_marks):

    texts_embeddings = text_encoder.encode(texts)

    sender = np.array(sender_marks).reshape(-1,1)

    embeddings = np.hstack([np.vstack(texts_embeddings),sender])

    return embeddings

def custom_collate(batch):


    features = [torch.Tensor(item[0]) for item in batch]
    sender_marks_dialogs_items = [item[1] for item in batch]
    texts = [item[2] for item in batch]
    categories = [item[3] for item in batch]

    user_messages_indexes = [[i for i in range(len(sender_marks)) if sender_marks[i]] for sender_marks in sender_marks_dialogs_items]

    seq_lens = [len(sender_mark) for sender_mark in sender_marks_dialogs_items]

    for idx in range(len(features)):
        #test sizes is corresponding each other
        assert features[idx].shape[0] == len(sender_marks_dialogs_items[idx])

    features = torch.nn.utils.rnn.pad_sequence(features,
                                               batch_first=True,
                                               padding_value=0)

    return features, sender_marks_dialogs_items, categories, user_messages_indexes, seq_lens, texts

class DialogDatasetEmbedded(Dataset):
    def __init__(self,
                 text_encoder,
                 category2index,
                 dialogs,
                 sender_field = "sender",
                 sender_mark = "user"
                 ):

        self.dialogs = dialogs
        self.text_encoder = text_encoder
        self.category2index = category2index
        self.sender_field = sender_field
        self.sender_mark = sender_mark

    def __len__(self):
        return len(self.dialogs)

    def __getitem__(self, idx):

        dialog = self.dialogs[idx]
        dialog.sort(key = lambda item : item['date'])

        texts = [item['text'] for item in dialog]
        sender = [int(item[self.sender_field] == self.sender_mark) for item in dialog]

        features = encode_features(self.text_encoder, texts, sender)

        categories = [self.category2index[item['category']] if item['category'] else 0 for item in self.dialogs[idx]]

        return features, sender, texts, categories



category2index = {
    'sleep': 1,
    'food': 2,
    'water': 3,
    'stress': 4,
    'workout': 5,
    'weight': 0,
    0:0
}

if __name__ == '__main__':

    # dataset = DialogDatasetEmbedded(text_encoder,category2index,dialogs.pkl)
    # validation_dataset = DialogDatasetEmbedded(text_encoder, category2index, validation_dialogs)


    text_encoder = SentenceTransformer('distilbert-base-nli-mean-tokens', device='cuda')

    dialogs = pd.read_pickle('data/filtered.pkl')
    dialogs = pd.DataFrame(dialogs)['dialog'].tolist()
    train, test = train_test_split(dialogs,
                                  random_state = 42,
                                  shuffle = True,
                                  test_size = 0.2)

    train_dataset = DialogDatasetEmbedded(text_encoder,
                                          category2index,
                                          train,
                                          sender_field = "name",
                                          sender_mark = "received")

    test_dataset = DialogDatasetEmbedded( text_encoder,
                                          category2index,
                                          test,
                                          sender_field = "name",
                                          sender_mark = "received")

    train_loader = DataLoader(train_dataset,
                              batch_size = 32,
                              collate_fn = custom_collate,
                              shuffle=False)

    test_loader = DataLoader( test_dataset,
                              batch_size = 32,
                              collate_fn = custom_collate,
                              shuffle=False)



